import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ActivityThreeTests {

    //Location of chromedriver file
    final String CHROMEDRIVER_LOCATION = "/Users/navpreetkaur/Desktop/chromedriver";
   
    //Website we wwant to test
    final String URL_TO_TEST = "https://www.mcdonalds.com/ca/en-ca.html";
    
    //Global Driver declaration
    WebDriver driver;       
    
    
	@Before
	public void setUp() throws Exception {
	 //Setup Selenium
	  System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
	                
	 //Instantiate object of ChromeDriver
	     driver = new ChromeDriver();
	                
	 //Go to website
	     driver.get(URL_TO_TEST);
	}


    // TC1:  Title of the subscription feature is “Subscribe to my Mcd’s®”
    
	   @Test
	    public void testCaseOne() {   
	     // Get the access to the title text
	        WebElement heading = driver.findElement(By.cssSelector("h2.click-before-outline"));
	        String headingText = heading.getText();
	        
	     // System.out.println(headingText);
	        
	        String expectedResult = "Subscribe to My McD’s®";
	        
	     //Testing...
	        assertEquals(expectedResult,headingText);
	        
	        
	        System.out.println("test case 1 done");
	        
	    }
	   
	   
	   //TC2:  Email signup - happy path  (Clicking on Subscribe & then Verify Button - some error message should be displayed
	   
	    @Test
	     public void testCaseTwo() {
	        
	        //Fetch all the input boxes
	        WebElement firstName = driver.findElement(By.id("firstname2"));
	        WebElement emailAddress = driver.findElement(By.id("email2"));
	        WebElement postalCode = driver.findElement(By.id("postalcode2"));
	        
	        
	        //Filling in the form
	        firstName.sendKeys("Navneet");
	        emailAddress.sendKeys("neer.multani@gmail.com");
	        postalCode.sendKeys("MIT");
	        
	        //Clicking the Subscribe button
	        WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
	        subscribeButton.click();
	        System.out.println("Subscribe Button Clicked");
	        
	        
	        
	        /* Couldn't fetch the id of the Recaptcha Verify Button as it is coming from Google APIs
	        
	        
	        // Getting the Verify Button's Id 
             WebElement verifyButton = driver.findElement(By.id("g-recaptcha-btn"));
             verifyButton.click();
             System.out.println("Verify Button Clicked");
	        
	        //Getting the Error Message's Text
	        
	         WebElement verifyTextShown = driver.findElement(By.className("g-recaptcha-response-1"));
	         String verifyTextString = verifyTextShown.getText();    
	        //System.out.println(verifyTextString);
	    
	       
	        String expectedResult = "Please select all matching images.";
	        
	        //Testing Now...
	        assertEquals(expectedResult,verifyTextString);
	        
	         */
	    }
	    
	    
	    //TC3: Getting error messages for empty input text boxes
	    
	    @Test 
	    public void testCaseThree() {
	    	
            //Getting access to all the Input Text Boxes
            WebElement firstName = driver.findElement(By.id("firstname2"));
            WebElement lastName = driver.findElement(By.id("email2"));
            WebElement postalCode = driver.findElement(By.id("postalcode2"));
            
            
            //Sending the keys
            firstName.sendKeys("");
            lastName.sendKeys("");
            postalCode.sendKeys("");
            
            //Clicking the Subscribe Button
            WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
            subscribeButton.click();
            
            
            // Same Problem faced as in Test Case 2 -> Couldn't fetch Id for Recaptcha Verify Button
	    }

}
